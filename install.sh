#!/bin/sh

echo "Setting up the mac..."

# Update Homebrew recipes
brew update

# Install all dependencies with bundle (See Brewfile)
brew tap homebrew/bundle
brew bundle

# Set default MySQL root password and auth type.
brew services start mysql@5.7
mysql -u root -e "ALTER USER root@localhost IDENTIFIED WITH mysql_native_password BY 'root'; FLUSH PRIVILEGES;"

# Install PHP extensions with PECL
pecl install memcached imagick

# Install global Composer packages
/usr/local/bin/composer global require laravel/installer laravel/valet

# Install cgr (see: https://packagist.org/packages/consolidation/cgr)
/usr/local/bin/composer global require consolidation/cgr

# Install Laravel Valet
$HOME/.composer/vendor/bin/valet install

# Removes .zshrc from $HOME (if it exists) and symlinks the .zshrc file from the .dotfiles
rm -rf $HOME/.zshrc
ln -s $HOME/.dotfiles/.zshrc $HOME/.zshrc

# Setup dock
source setup-dock.sh

# Restore app config
source mackup-restore.sh

# Set macOS preferences
# We will run this last because this will reload the shell
source .macos

echo "Finished setup."