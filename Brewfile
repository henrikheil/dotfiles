tap "homebrew/bundle"
tap "homebrew/cask"
tap 'homebrew/cask-fonts'
tap "homebrew/cask-versions"
tap "homebrew/core"
tap "homebrew/services"

brew 'bash'
brew 'bat'
brew 'coreutils'
brew 'httpie'
brew 'mas' # Mac App Store manager
brew 'tree'
brew 'zlib'

# Development
brew 'php'
brew 'php@7.3'
brew 'composer'
brew 'imagemagick'
brew 'mysql@5.7', link: true
brew 'nginx'
brew 'node'
brew 'yarn'
brew 'mailhog', restart_service: true
brew 'selenium-server-standalone'
brew 'dnsmasq'
brew 'circleci'
brew 'mas'
brew 'wget'

# Cask
cask 'alfred'
cask 'brave-browser'
cask 'bitwarden'
cask 'balenaetcher'
cask 'bartender'
cask 'calibre'
cask 'cleanmymac'
cask 'coconutbattery'
cask 'coderunner'
cask 'deltawalker'
cask 'discord'
cask 'docker'
cask 'electrum'
cask 'elmedia-player'
cask 'firefox'
cask 'forklift'
cask 'google-backup-and-sync'
cask 'google-chrome'
cask 'handbrake'
cask 'imageoptim'
cask 'iterm2'
cask 'java'
cask 'kindle'
cask 'knockknock'
cask 'lingon-x'
cask 'little-snitch'
cask 'mamp'
cask 'nvalt'
cask 'onyx'
cask 'paw'
cask 'pdf-expert'
cask 'phpstorm'
cask 'signal'
cask 'sketch'
cask 'slack'
cask 'sourcetree'
cask 'sublime-text'
cask 'tableplus'
cask 'tidal'
cask 'toggl'
cask 'the-unarchiver'
cask 'transmit'
cask 'vagrant'
cask 'virtualbox' # Might need to allow access in Security settings
cask 'viscosity'
cask 'visual-studio-code'

# Fonts
cask 'font-lato'
cask 'font-open-sans'
cask 'font-roboto'
cask 'font-roboto-condensed'
cask 'font-source-code-pro-for-powerline'
cask 'font-source-code-pro'
cask 'font-source-sans-pro'
cask 'font-source-serif-pro'

# Mac Store
mas "Amphetamine", id: 937984704
mas "Disk Speed Test", id: 425264550
mas "Display Menu", id: 549083868
mas "Magnet", id: 441258766
mas "Microsoft To Do", id: 1274495053
mas "Monosnap", id: 540348655
mas "SiteSucker", id: 442168834