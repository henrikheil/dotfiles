brew install dockutil

dockutil --remove all --no-restart
dockutil --add "/Applications/PhpStorm.app" --no-restart
dockutil --add "/Applications/MAMP PRO.app" --no-restart
dockutil --add "/Applications/Brave Browser.app" --no-restart
dockutil --add "/Applications/Tidal.app" --no-restart

killall Dock
wait 2